import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DarkModeService {

  darkMode: boolean = false;

  constructor() { }

  public setDarkTheme(){
    this.darkMode = !this.darkMode;
    document.body.classList.toggle('dark');
  }
}
