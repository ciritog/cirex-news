import { Component, ViewChild, OnInit } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { NoticiasService } from '../../services/noticias.service';
import { Article } from '../../interfaces/interfaces';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  @ViewChild(IonSegment) segment: IonSegment;

  categoriaActual = '';
  categorias = ['finanzas', 'entretenimiento', 'general', 'tecnología', 'ciencia', 'salud', 'deportes'];
  noticias: Article[] = [];

  constructor(private noticiasService: NoticiasService) {}

  ngOnInit(){

    
    this.cargarNoticias(this.traducirCategoria(this.categorias[0]));
        
  }

  traducirCategoria( categoria:string ){
    let categoriaEn = categoria;

    switch (categoria) {
      case 'finanzas':
        categoriaEn = 'business';
        break;
      case 'entretenimiento':
        categoriaEn = 'entertainment';
        break;
      case 'salud':
        categoriaEn = 'health';
        break;
      case 'ciencia':
        categoriaEn = 'science';
        break;
      case 'deportes':
        categoriaEn = 'sports';
        break;
      case 'tecnología':
        categoriaEn = 'technology';
        break;
    }

    return categoriaEn;
  }

  cambioCategoria(event){
    
    this.noticias = [];

    this.categoriaActual = this.traducirCategoria( event.detail.value );

    this.cargarNoticias( this.categoriaActual );
  }

  cargarNoticias( categoria: string, event?){
    this.noticiasService.getTopHeadlinesCategoria( categoria )
    .subscribe(resp => {
      this.noticias.push(...resp.articles); 

      if(resp.articles.length === 0){
        event.target.disabled = true;
        event.target.complete();
        return;
      }

      if(event){
        event.target.complete();
      }

    });
  }

  loadData(event){
    this.cargarNoticias( this.categoriaActual, event );
  }

}
