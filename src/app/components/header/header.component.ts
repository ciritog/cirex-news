import { Component, OnInit, Input } from '@angular/core';
import { DarkModeService } from '../../services/dark-mode.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() seccion: string;
  @Input() color = 'primary';

  constructor(public darkModeService: DarkModeService) { 
  }

  ngOnInit() {}

}
